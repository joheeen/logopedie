//
//  GameBoardView.swift
//  Logopedie
//
//  Created by Johan van der Meulen on 03/11/15.
//  Copyright © 2015 TNW5. All rights reserved.
//

import Foundation
import UIKit

class GameBoardView: UIView {
    
    private var boardView: UIView?
    private var boardViewBlocks = [UIView]()
    
    //Holds the variables for the dashboard to init
    var initiliasedDrawBlocks = [DrawBlock]()
    
    init(boardSize: UIView) {
        //Initialising the gameBoard
        super.init(frame: boardSize.frame)
        backgroundColor = UIColor.blueColor()
        
        //Table settings //Warning. These settings need to be possible
        let tableSettings = getTableSettings(GameSettings.difficultyLvl)
        let numberOfColumns = tableSettings.numberOfColumns
        let numberOfBlocks = tableSettings.numberOfColumns * tableSettings.numberOfRows
        
        //Setting the CGRect size
        let numberOfRows = Double(numberOfBlocks / numberOfColumns)
        let cgRectSize = CGSize(width: Double(frame.width) / Double(numberOfColumns), height: Double(frame.height) / Double(numberOfRows))
        
        //Create a random list of image names
        let randomStringsOfimages = DrawBlockContentGenerator().generateRandomListOfStringImages(numberOfBlocks)
        
        //Create the drawblocks
        for index in 0...(numberOfBlocks - 1) {
            
            //Column and row
            let row = floor( Double(index / numberOfColumns) )
            let column = Double(index) - ( Double(numberOfColumns) * row )
            
            //Position
            let columnPosition = Double(cgRectSize.width) * column
            let rowPosition = Double(cgRectSize.height) * row
            
            let position = CGPoint(x: columnPosition, y: rowPosition)
            let cgRect = CGRect(origin: position, size: cgRectSize)
            
            //Setting Gesture Recognition
            let drawBlock = DrawBlock(frame: cgRect)
            drawBlock.backgroundColor = UIColor.blackColor()
            drawBlock.adjustsImageWhenHighlighted = false
            
            //Setting the image of the buttons
            let image = UIImage(named: randomStringsOfimages[index])! as UIImage
            drawBlock.setBackgroundImage(image, forState: UIControlState.Normal)

            drawBlock.id = index
            drawBlock.imageName = randomStringsOfimages[index]
            
            //Send the trigger to the controller
            drawBlock.addTarget(self.superview, action: "blockTouched:", forControlEvents: UIControlEvents.TouchUpInside)
            
            //Fill the array with drawBlocks
            initiliasedDrawBlocks.append(drawBlock)
            
            addSubview(drawBlock)
        }
        print("Initialised GameBoard")
    }
    
    //Table size
    func getTableSettings(dif: difficulty) -> (numberOfRows: Int, numberOfColumns: Int)
    {
            var tableSettings = (Int(), Int())
            
            switch (dif) {
            case difficulty.easy:
                tableSettings = (3, 4)
            break
        
            case difficulty.medium:
                tableSettings = (4, 6)
            break
                
            case difficulty.hard:
                //TODO: Change back to 5, 5. nil error because of to less assets
                tableSettings = (3, 5)
            break
        }
        
        return tableSettings
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}