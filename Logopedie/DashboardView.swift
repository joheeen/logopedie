//
//  DashboardView.swift
//  Logopedie
//
//  Created by Johan van der Meulen on 04/11/15.
//  Copyright © 2015 TNW5. All rights reserved.
//

import Foundation
import UIKit

class DashboardItem: UIImageView {
    var id: Int?
    var found = false
}

class DashboardView: UIView {
    
    var dashboardItems = [DashboardItem]()
    
    init(dashboardSize: UIView, blocksToFind: [DrawBlock]) {
        //Initialising the gameBoard
        super.init(frame: dashboardSize.frame)
        print("Dashboardview initialised")
        backgroundColor = UIColor.redColor()
        
        //Initialising stackView
        let stackView = UIStackView(frame: self.frame)
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .Horizontal
        stackView.distribution = .EqualSpacing
        
        //Initialising stackView items
        for index in 0...blocksToFind.count - 1
        {
            //Setting size and initialising
            let cgSize = CGSize(width: self.frame.height, height: self.frame.height)
            let dashboardItem = DashboardItem()
            
            //Setting constraints
            dashboardItem.constrainToSize(cgSize)

            //Setting the image
            let image = UIImage(named: blocksToFind[index].imageName!)! as UIImage
            dashboardItem.image = image
            
            //Setting the id for the controller to use
            dashboardItem.id = blocksToFind[index].id
            
            //Add to stackView
            dashboardItems.append(dashboardItem)
            stackView.addArrangedSubview(dashboardItem)
        }
        
        //Constraints
        self.addSubview(stackView)
        
        //Add constraints from stackview to the dashboardview
        stackView.leftAnchor.constraintEqualToAnchor(self.leftAnchor).active = true
        stackView.rightAnchor.constraintEqualToAnchor(self.rightAnchor).active = true
        stackView.topAnchor.constraintEqualToAnchor(self.topAnchor).active = true
        stackView.bottomAnchor.constraintEqualToAnchor(self.bottomAnchor).active = true
        
        NSLayoutConstraint(item: stackView, attribute: .Width, relatedBy: NSLayoutRelation.Equal, toItem: self, attribute: .Width, multiplier: 1, constant: 0).active = true
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}