//
//  InputViewController.swift
//  Logopedie
//
//  Created by Johan van der Meulen on 03/11/15.
//  Copyright © 2015 TNW5. All rights reserved.
//

import Foundation
import UIKit

class InputViewController: UIViewController, UIGestureRecognizerDelegate {
    
    let tapInputRecognizer = UITapGestureRecognizer()
    
    override func viewDidLoad() {
        //initial method
        tapInputRecognizer.delegate = self
        
        tapInputRecognizer.addTarget(self, action: "TouchRecognized")
        
        view.addGestureRecognizer(tapInputRecognizer)
        print("View loaded")
    }
    
    //UITapGestureRecognizer
    func TouchRecognized() {
        print("Touch recognized")
    }
    
    //UIGestureRecognizerDelegate
    func gestureRecognizer(gestureRecognizer: UIGestureRecognizer, shouldReceiveTouch touch: UITouch) -> Bool {
        
        print("Gesture recognizer triggered")
        
        return true
    }
    
//    func draggedView(sender:UIPanGestureRecognizer){
//        self.view.bringSubviewToFront(sender.view!)
//        let translation = sender.translationInView(self.view)
//        sender.view!.center = CGPointMake(sender.view!.center.x + translation.x, sender.view!.center.y + translation.y)
//        sender.setTranslation(CGPointZero, inView: self.view)
//    }
}