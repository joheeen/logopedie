//
//  InputViewController.swift
//  Logopedie
//
//  Created by Johan van der Meulen on 03/11/15.
//  Copyright © 2015 TNW5. All rights reserved.
//

import Foundation
import UIKit

class GameSettings {
    static var difficultyLvl = difficulty.easy
}

//This class manages the game and the views in it
class GameViewController: UIViewController, UIGestureRecognizerDelegate {
    
    //Gesture Recognizer
    let tapInputRecognizer = UITapGestureRecognizer()
    
    //Boardview declaration
    var boardView: GameBoardView?
    var drawBlocks: [DrawBlock]?
    var blocksToFind: [DrawBlock]?
    
    //DashboardView declaration
    var dashboardView: DashboardView?
    
    override func viewDidLoad() {
        //Add touch input recognition
        tapInputRecognizer.delegate = self
        
        //Boardview initialising
        boardView = GameBoardView(boardSize: self.view)
        view.addSubview(boardView!)
        drawBlocks = boardView?.initiliasedDrawBlocks
        print("Gameview loaded")
    
        //DashboardView initialising
        blocksToFind = getBlocksToFind()
        //Print debug
        print("Blocks to find")
        for index in 0...blocksToFind!.count - 1 {
            print("\(blocksToFind![index].id)")
        }
        dashboardView = DashboardView(dashboardSize: self.view, blocksToFind: blocksToFind!)
//        view.addSubview(dashboardView!)
        print("Dashboard loaded")
    }

    //Is triggered from the GameBoardView
    func blockTouched(sender: DrawBlock) {
        //draw block is pressed
        print("drawblock id: " + "\(sender.id)" + " pressed")
        
        for drawBlock in blocksToFind! {
            if drawBlock.id == sender.id {
                print("Jeej")
            }
        }
    }
    
    
    func getBlocksToFind() -> [DrawBlock] {
        var amountOfBlocksToFind = Int()
        var drawBlocksToFind = [DrawBlock]()
        
        switch (GameSettings.difficultyLvl) {
            
        case difficulty.easy:
            amountOfBlocksToFind = 1
            break
        case difficulty.medium:
            amountOfBlocksToFind = 2
            break
        case difficulty.hard:
            amountOfBlocksToFind = 3
            break
        }
        
        for _ in 1...amountOfBlocksToFind {
            drawBlocksToFind.append(drawBlocks![Random.randomInt(0, max: (drawBlocks?.count)! - 1)])
        }
        
        return drawBlocksToFind
    }
//    func draggedView(sender:UIPanGestureRecognizer){
//        self.view.bringSubviewToFront(sender.view!)
//        let translation = sender.translationInView(self.view)
//        sender.view!.center = CGPointMake(sender.view!.center.x + translation.x, sender.view!.center.y + translation.y)
//        sender.setTranslation(CGPointZero, inView: self.view)
//    }
}