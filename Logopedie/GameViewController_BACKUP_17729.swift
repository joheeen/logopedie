//
//  InputViewController.swift
//  Logopedie
//
//  Created by Johan van der Meulen on 03/11/15.
//  Copyright © 2015 TNW5. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation

class GameSettings {
    static var difficultyLvl = difficulty.medium
    
    static var gameboardHeight = 0.7
}

//This class manages the game and the views in it
class GameViewController: UIViewController, UIGestureRecognizerDelegate {
    
    //Gesture Recognizer
    let tapInputRecognizer = UITapGestureRecognizer()
    
    //Boardview declaration
    var boardView: GameBoardView?
    var drawBlocks: [DrawBlock]?
    var blocksToFind: [DrawBlock]?
    var audioPlayer:AVAudioPlayer = AVAudioPlayer()
    
    //DashboardView declaration
    var dashboardView: DashboardView?
    
    override func viewDidLoad() {
        //Add touch input recognition
        tapInputRecognizer.delegate = self
        
        //Boardview size
        let boardViewFrame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height * CGFloat(GameSettings.gameboardHeight))
        let boardViewSize = UIView(frame: boardViewFrame)
        //Boardview initialising
        boardView = GameBoardView(boardSize: boardViewSize)
        view.addSubview(boardView!)
        drawBlocks = boardView?.initiliasedDrawBlocks
        print("Gameview loaded")
    
        //DashboardView initialising
        blocksToFind = getBlocksToFind()
        //Print debug
        print("Blocks to find")
        for index in 0...blocksToFind!.count - 1 {
            print("\(blocksToFind![index].id)")
        }
        //Dashboard view size
        let dashboardViewFrame = CGRect(x: 0, y: boardViewFrame.height, width: self.view.frame.width, height: view.frame.height - boardViewFrame.height)
        let dashboardViewSize = UIView(frame: dashboardViewFrame)

        dashboardView = DashboardView(dashboardSize: dashboardViewSize, blocksToFind: blocksToFind!)
        view.addSubview(dashboardView!)
        
        print("Dashboard loaded")
    }

    //Is triggered from the GameBoardView
    func blockTouched(sender: DrawBlock) {
        //draw block is pressed
        print("drawblock id: " + "\(sender.id)" + " pressed")
        
        for drawBlock in blocksToFind! {
            if drawBlock.id == sender.id {
<<<<<<< HEAD
                
                drawBlock.adjustsImageWhenHighlighted = true
                drawBlock.highlighted = true
                
                for dashboardItem in (dashboardView?.dashboardItems)! {
                    if dashboardItem.id == sender.id {
                        dashboardItem.textColor = UIColor.greenColor()
                    }
                }
            }
        }
    }
=======
                print("Jeej")
                let audioPlayerURL:NSURL = NSBundle.mainBundle().URLForResource("cheers_25", withExtension: "m4a")!
                do { audioPlayer = try AVAudioPlayer(contentsOfURL: audioPlayerURL, fileTypeHint: nil) } catch _ {return}
                    audioPlayer.numberOfLoops = 0
                    audioPlayer.prepareToPlay()
                    audioPlayer.play()
                    }
                }
            }
>>>>>>> feature/Audio
    
    func getBlocksToFind() -> [DrawBlock] {
        var amountOfBlocksToFind = Int()
        var drawBlocksToFind = [DrawBlock]()
        
        switch (GameSettings.difficultyLvl) {
            
        case difficulty.easy:
            amountOfBlocksToFind = 1
            break
        case difficulty.medium:
            amountOfBlocksToFind = 2
            break
        case difficulty.hard:
            amountOfBlocksToFind = 3
            break
        }
        
        for _ in 1...amountOfBlocksToFind {
            drawBlocksToFind.append(drawBlocks![Random.randomInt(0, max: (drawBlocks?.count)! - 1)])
        }
        
        return drawBlocksToFind
    }
}