//
//  AppDelegate.swift
//  Logopedie
//
//  Created by Johan van der Meulen on 03/11/15.
//  Copyright © 2015 TNW5. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        window = UIWindow(frame: UIScreen.mainScreen().bounds)
        
        let inputViewController = GameViewController()
        inputViewController.view.backgroundColor = UIColor.greenColor()
        inputViewController.title = "InputController"
        
//        let tab = UITabBarController()
//        tab.viewControllers = [inputViewController]
//        window?.rootViewController = tab

        
        window?.rootViewController = inputViewController
        window?.makeKeyAndVisible()
        
        return true
    }
}

