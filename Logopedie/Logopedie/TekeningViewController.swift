//
//  TekeningViewController.swift
//  Logopedie
//
//  Created by CMD on 03/11/15.
//  Copyright © 2015 TNW5. All rights reserved.
//

import UIKit

class TekeningViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // tekeningView is the upperside of the application. The drawings will be shown in here.
        let tekeningView = UIView(frame: view.frame)
        tekeningView.backgroundColor = UIColor.redColor()
        
        self.view.addSubview(tekeningView)
        
        // This part is responsible for the positioning of the drawings inside the tekeningView.
        _ = NSLayoutConstraint(item: tekeningView, attribute: .Width, relatedBy: NSLayoutRelation.Equal, toItem: self.view, attribute: .Width, multiplier: 1, constant: 0).active = true
        _ = NSLayoutConstraint(item: tekeningView, attribute: .Height, relatedBy: NSLayoutRelation.Equal, toItem: self.view, attribute: .Height, multiplier: 2/3, constant: 0).active = true
        
        tekeningView.translatesAutoresizingMaskIntoConstraints = false
        
//        // The objectView is the second part of the screen, in here you will find the objects you have to find in the tekeningView.
        let stackViewContainer = UIView(frame: view.frame)
        stackViewContainer.backgroundColor = UIColor.greenColor()
        
        self.view.addSubview(stackViewContainer)

        // This part is responsible for the positioning of the objects inside the objectView.
        stackViewContainer.topAnchor.constraintEqualToAnchor(tekeningView.bottomAnchor).active = true
        _ = NSLayoutConstraint(item: stackViewContainer, attribute: .Width, relatedBy: NSLayoutRelation.Equal, toItem: self.view, attribute: .Width, multiplier: 1, constant: 0).active = true
        _ = NSLayoutConstraint(item: stackViewContainer, attribute: .Height, relatedBy: NSLayoutRelation.Equal, toItem: self.view, attribute: .Height, multiplier: 1/3, constant: 0).active = true
        stackViewContainer.translatesAutoresizingMaskIntoConstraints = false
        
        //Stackview
        let objectStackView = UIStackView()
        objectStackView.translatesAutoresizingMaskIntoConstraints = false
        objectStackView.axis = .Horizontal
        objectStackView.distribution = .EqualCentering

        let numberOfItems = 5
        for index in 1...numberOfItems {
            let cgRect = CGRectMake(100, 100, 100, 100)
            let object = UIView(frame: cgRect)
            object.backgroundColor = UIColor.grayColor()
            print ("\(index)")
            objectStackView.addArrangedSubview(object)
        }
        
        stackViewContainer.addSubview(objectStackView)
        
        objectStackView.centerYAnchor.constraintEqualToAnchor(stackViewContainer.centerYAnchor).active = true
        objectStackView.centerXAnchor.constraintEqualToAnchor(stackViewContainer.centerXAnchor).active = true
        
        
    }
}
    
    
    






