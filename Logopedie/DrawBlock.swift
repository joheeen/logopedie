//
//  DrawBlock.swift
//  Logopedie
//
//  Created by Johan van der Meulen on 04/11/15.
//  Copyright © 2015 TNW5. All rights reserved.
//

import Foundation
import UIKit

class DrawBlock: UIButton {
    var id: Int?
    var imageName: String?
}

class DrawBlockContentGenerator {
    //List of pictures 
    //Add the names of new content here!
    var listOfImages: [String] = ["De appel.png", "De auto.png", "De boom.png", "De fiets.png", "De fles.png", "De glijbaan.png", "De hond.png", "De kat.png", "De koffer.png", "De kraan.png", "De plant.png", "De sleutel.png", "De stoel.png", "De stofzuiger.png", "De taart.png", "De tablet.png", "De voetbal.png", "De vork.png", "Het beeldscherm.png", "Het huis.png", "Het mes.png", "De tandenborstel.png", "De muis.png"]
    
    //Generates list of random strings to create UIimages
    func generateRandomListOfStringImages(amount: Int) -> [String] {
     
        var imageNames = [String]()
        var numbers = Random.randomIntsWithoutDuplicates(0, max: listOfImages.count - 1)
        
        for _ in 0...amount - 1 {
            let index = numbers.first
            
            imageNames.append(listOfImages[index!])
            numbers.removeFirst()
        }
        
        return imageNames
    }
}

