//
//  InputViewController.swift
//  Logopedie
//
//  Created by Johan van der Meulen on 03/11/15.
//  Copyright © 2015 TNW5. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation

class GameSettings {
    static var difficultyLvl = difficulty.hard
    
    static var gameboardHeight = 0.75
}

enum difficulty {
    case easy
    case medium
    case hard
}

//This class manages the game and the views in it
class GameViewController: UIViewController, UIGestureRecognizerDelegate {
    
    //Gesture Recognizer
    let tapInputRecognizer = UITapGestureRecognizer()
    
    //Audio
    let audioPlayer = AudioPlayer()
    
    //Boardview declaration
    var boardView: GameBoardView?
    var drawBlocks: [DrawBlock]?
    var blocksToFind: [DrawBlock]?
    
    //DashboardView declaration
    var dashboardView: DashboardView?
    
    //Init game
    override func viewDidLoad() {
        //Add touch input recognition
        tapInputRecognizer.delegate = self
        
        //Boardview size
        let boardViewFrame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height * CGFloat(GameSettings.gameboardHeight))
        let boardViewSize = UIView(frame: boardViewFrame)
        //Boardview initialising
        boardView = GameBoardView(boardSize: boardViewSize)
        view.addSubview(boardView!)
        drawBlocks = boardView?.initiliasedDrawBlocks
        print("Gameview loaded")
        print("boardViewFrame " + "\(boardViewFrame)")
        
        //DashboardView initialising
        blocksToFind = getBlocksToFind()

        //Dashboard view size
        let dashboardViewFrame = CGRect(x: 0, y: boardViewFrame.height, width: self.view.frame.width, height: view.frame.height - boardViewFrame.height)
        let dashboardViewSize = UIView(frame: dashboardViewFrame)
        print("DashboardViewFrame " + "\(dashboardViewFrame)")
        
        dashboardView = DashboardView(dashboardSize: dashboardViewSize, blocksToFind: blocksToFind!)
        view.addSubview(dashboardView!)
        
        print("Dashboard loaded")
    }

    //Is triggered from the GameBoardView
    func blockTouched(sender: DrawBlock) {
        //draw block is pressed
        print("drawblock id: " + "\(sender.id)" + " pressed")
        
        //Loop through the blocks
        for drashboardBlock in blocksToFind!
        {
            //If a known drawblock is the same as the senders id
            if drashboardBlock.id == sender.id
            {
                //init var
                var allBlockFound = true
                
                //Set the dashboard item appearance
                for dashboardItem in (dashboardView?.dashboardItems)!
                {
                    if dashboardItem.id == sender.id {
                        //Change image appearance
                        dashboardItem.image = UIImage()
                        dashboardItem.backgroundColor  = UIColor.greenColor()
                     
                        //Set dashboard item to found
                        dashboardItem.found = true
                    }
                    
                    //Check if all items are found
                    if !dashboardItem.found {
                        allBlockFound = false
                    }
                }
                //Audio
                audioPlayer.Play("cheers_25", fileFormat: "m4a")
                
                //End of game
                if (allBlockFound) {
                    print ("All blocks found")
                    
                    //Wait for 1 second to execute the func
                    let time = dispatch_time(dispatch_time_t(DISPATCH_TIME_NOW), 1 * Int64(NSEC_PER_SEC))
                    dispatch_after(time, dispatch_get_main_queue()) {
                        self.viewDidLoad()
                    }
                }
            }
        }
    }
    
    func getBlocksToFind() -> [DrawBlock] {
        var amountOfBlocksToFind = Int()
        var drawBlocksToFind = [DrawBlock]()
        
        switch (GameSettings.difficultyLvl) {
            
        case difficulty.easy:
            amountOfBlocksToFind = 1
            break
        case difficulty.medium:
            amountOfBlocksToFind = 2
            break
        case difficulty.hard:
            amountOfBlocksToFind = 3
            break
        }
        
        var numbers = Random.randomIntsWithoutDuplicates(0, max: (drawBlocks?.count)! - 1)
        for _ in 0...amountOfBlocksToFind - 1 {
            drawBlocksToFind.append(drawBlocks![numbers.first!])
            numbers.removeFirst()
        }
        
        return drawBlocksToFind
    }
}