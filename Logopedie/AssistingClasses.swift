//
//  AssistingClasses.swift
//  Logopedie
//
//  Created by Johan van der Meulen on 04/11/15.
//  Copyright © 2015 TNW5. All rights reserved.
//

import Foundation

class Random {
    static func randomInt(min: Int, max:Int) -> Int {
        return min + Int(arc4random_uniform(UInt32(max - min + 1)))
    }
    
    static func randomIntsWithoutDuplicates(min: Int, max:Int) -> [Int] {
        
        var numbers = [Int]()
        
        for index in min...max {
            numbers.append(index)
        }
        
        return numbers.shuffle()
    }
}

extension CollectionType {
    /// Return a copy of `self` with its elements shuffled
    func shuffle() -> [Generator.Element] {
        var list = Array(self)
        list.shuffleInPlace()
        return list
    }
}

extension MutableCollectionType where Index == Int {
    /// Shuffle the elements of `self` in-place.
    mutating func shuffleInPlace() {
        // empty and single-element collections don't shuffle
        if count < 2 { return }
        
        for i in 0..<count - 1 {
            let j = Int(arc4random_uniform(UInt32(count - i))) + i
            guard i != j else { continue }
            swap(&self[i], &self[j])
        }
    }
}

import UIKit

extension UIView {
    func centerInView (view : UIView) {
        let horizontalConstraint = self.centerXAnchor.constraintEqualToAnchor(view.centerXAnchor)
        let vertivalConstraint = self.centerYAnchor.constraintEqualToAnchor(view.centerYAnchor)
        NSLayoutConstraint.activateConstraints([horizontalConstraint, vertivalConstraint, ])
        
    }
    
    func constrainToSize (size : CGSize) {
        let widthConstraint = self.widthAnchor.constraintEqualToAnchor(nil, constant: size.width)
        let heightConstraint = self.heightAnchor.constraintEqualToAnchor(nil, constant: size.height)
        NSLayoutConstraint.activateConstraints([widthConstraint, heightConstraint])
        
    }
}

import AVFoundation

class AudioPlayer {
    //Audio var needs to be out of the function scope to work
    var audioPlayer: AVAudioPlayer?
    
    init () {
        audioPlayer = AVAudioPlayer()
    }
    
    func Play(title: String, fileFormat: String)
    {
        let audioPlayerURL:NSURL = NSBundle.mainBundle().URLForResource(title, withExtension: fileFormat)!
        
        do
        {
            audioPlayer = try AVAudioPlayer(contentsOfURL: audioPlayerURL, fileTypeHint: nil)
        }
        catch _
        {
            return
        }
        
        audioPlayer!.prepareToPlay()
        audioPlayer!.play()
    }
}
